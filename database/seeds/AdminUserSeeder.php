<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AdminUserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create( [
            'id'       => 1,
            'name'     => config( 'admin.name' ),
            'email'    => config( 'admin.email' ),
            'password' => Hash::make( config( 'admin.password' ) ),
            'admin'   => 1
        ] );
    }
}
