<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $statuses = collect( [ 'Active', 'Pending', 'Inactive', 'Closed' ] );

        $statuses->each( function ( $item, $key ) {
            Status::create( [
                'name' => $item,
                'slug' => strtolower($item),
                'icon' => ''
            ] );
        } );

    }
}
