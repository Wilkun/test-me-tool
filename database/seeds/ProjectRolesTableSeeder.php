<?php

use App\Models\ProjectRole;
use Illuminate\Database\Seeder;

class ProjectRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectRole::create([
            'name' => 'Developer',
            'slug' => 'dev',
            'description' => 'Some code monkey. Give him a banana.'
        ]);

        ProjectRole::create([
            'name' => 'Tester',
            'slug' => 'tester',
            'description' => 'Bugs. My precioussss... bugs!!!'
        ]);
    }
}
