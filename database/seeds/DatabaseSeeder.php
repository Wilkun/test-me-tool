<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(ProjectRolesTableSeeder::class);
        $this->call(EnviromentsTableSeeder::class);
        $this->call(StatusesTableSeeder::class);

        if(in_array(config('app.env'), ['local', 'staging']) ) {
            $this->call(UsersTableSeeder::class);
            $this->call(ProjectsTableSeeder::class);
            $this->call(ProjectUserTableSeeder::class);
        }
    }
}
