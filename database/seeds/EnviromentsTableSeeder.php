<?php

use App\Models\Environment;
use Illuminate\Database\Seeder;

class EnviromentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Environment::create([
            'name' => 'Windows',
            'slug' => 'windows'
        ]);

        Environment::create([
            'name' => 'iOS',
            'slug' => 'apple'
        ]);

        Environment::create([
            'name' => 'Android',
            'slug' => 'android'
        ]);

        Environment::create([
            'name' => 'Web',
            'slug' => 'web'
        ]);
    }
}
