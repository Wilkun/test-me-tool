<?php

use Faker\Generator as Faker;

$factory->define( App\Models\ProjectUser::class, function ( Faker $faker ) {

    $projectId = $faker->numberBetween( 1, 20 );
    $userId = $faker->numberBetween( 1, 100 );

    $combo = $faker->unique()->regexify("^$projectId-$userId");

    $userId = explode('-', $combo)[1];

    return [
        'project_id' => $projectId,
        'user_id' => $userId,
        'role_id' => $faker->numberBetween( 1, 2 )
    ];
} );
