<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Project::class, function (Faker $faker) {
    return [
        'creator_id' => 1,
        'owner_id' => $faker->numberBetween(1,100),
        'environment_id' => $faker->numberBetween(1,4),
        'name' => $faker->text(20),
        'description' =>$faker->text(160),
        'status_id' => $faker->numberBetween(1,4),
        'deadline' => $faker->dateTimeBetween('now', '+2 years'),
        'deleted' => $faker->numberBetween(0,1)
    ];
});
