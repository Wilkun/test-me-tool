<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Status::class, function (Faker $faker) {
    $name = $faker->unique()->name;
    return [
        'name' => $faker->unique()->name,
        'slug' => strtolower($name),
        'icon' => ''
    ];
});
