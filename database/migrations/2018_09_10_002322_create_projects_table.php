<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('creator_id')->index();
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('owner_id')->index();
            $table->foreign('owner_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedInteger('environment_id')->index();
            $table->foreign('environment_id')->references('id')->on('environments')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->timestamp('deadline',0)->nullable();
            $table->unsignedInteger('status_id')->index();
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            $table->unique(['name', 'environment_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
