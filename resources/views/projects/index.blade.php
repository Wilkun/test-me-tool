@extends('layouts.auth')

@section('breadcrumbs')
    <li class="breadcrumb-item">Projects</li>
    <li class="breadcrumb-item active">Projects List</li>
@endsection

@section('content')
    <div class="card">
        <div class="body project_report">
            <div class="table-responsive">
                <table class="table table-hover js-basic-example dataTable table-custom m-b-0">
                    <thead>
                    <tr>
                        <th>{{ __('Project') }}</th>
                        <th>{{ __('Deadline') }}</th>
                        <th>{{ __('Progress') }}</th>
                        <th>{{ __('Lead') }}</th>
                        <th>{{ __('Team') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @each('projects.row',$projects,'project','projects.empty')
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--<div class="container">--}}


        {{--@includeWhen(count($owned) > 0, 'projects.table', [--}}
		{{--'projects' => $owned,--}}
		{{--'title'=>'My Projects'--}}
		{{--])--}}

        {{--@includeWhen(count($projects) > 0, 'projects.table', [--}}
		{{--'projects' => $projects,--}}
		{{--'title' => 'Team Projects'--}}
		{{--])--}}

        {{--@includeWhen($other, 'projects.table', [--}}
		{{--'projects' => $other,--}}
		{{--'title' => 'Other Projects'--}}
		{{--])--}}

    {{--</div>--}}
@endsection
