<li class="online">
    <a href="javascript:void(0);">
        <div class="media">
            <img class="media-object " src="{{ $member->get_gravatar($member->email, 90) }}" alt="">
            <div class="media-body">
                <span class="name">{{$member->name}}</span>
                <span class="message">Sales Lead</span>
            </div>
        </div>
    </a>
</li>
