<div class="row">
    <div class="col-ms-12">
        <div class="card w-100">
            <div class="card-header">{{ __($title) }}</div>

            <div class="card-body">
                <div class="card-content">

                    <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 20%">{{ __('Project Name')  }}</th>
                            <th>{{ __('Team Members')  }}</th>
                            <th>{{ __('Environment') }}</th>
                            <th>{{ __('Project Progress')  }}</th>
                            <th>{{ __('Status')  }}</th>
                            <th style="width: 20%">#{{ __('Edit')  }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 20%">{{ __('Project Name')  }}</th>
                            <th>{{ __('Team Members')  }}</th>
                            <th>{{ __('Environment') }}</th>
                            <th>{{ __('Project Progress')  }}</th>
                            <th>{{ __('Status')  }}</th>
                            <th style="width: 20%">#{{ __('Edit')  }}</th>
                        </tr>
                        </tfoot>
                        @each('projects.row',$projects,'project','projects.empty')
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
