<tr>
    <td class="project-title">
        <h6>{{ $project->name }}</h6>
        <small>Created {{ $project->created_at->diffForHumans() }}</small>
    </td>
    <td>{{ $project->deadline->diffForHumans() }}</td>
    <td>
        <div class="progress progress-xs">
            <div class="progress-bar" role="progressbar" aria-valuenow="48" aria-valuemin="0"
                 aria-valuemax="100" style="width: 48%;"></div>
        </div>
        <small>Completion with: 48%</small>
    </td>
    <td><img src="{{ $project->owner->get_gravatar($project->owner->email, 45) }}" data-toggle="tooltip" data-placement="top"
             title="{{ $project->owner->name }}" alt="Avatar" class="width35 rounded"></td>
    <td>
        @if(count($project->members)>0)
        <ul class="list-unstyled team-info">
            @each('projects.member-small',$project->members,'member')
        </ul>
        @endif
    </td>
    <td><span class="badge badge-{{$project->status->slug}}">{{$project->status->name}}</span></td>
    <td class="project-actions">
        @can('view', $project)
        <a href="{{action('ProjectController@show', ['id'=>$project->id])}}" class="btn btn-sm btn-outline-primary"><i
                class="icon-eye"></i></a>
        @endcan
        @can('update', $project)
        <a href="{{action('ProjectController@edit', ['id'=>$project->id])}}" class="btn btn-sm btn-outline-success"><i
                class="icon-pencil"></i></a>
        @endcan
        @can('delete', $project)
        <a href="{{action('ProjectController@destroy', ['id'=>$project->id])}}" class="btn btn-sm btn-outline-danger js-sweetalert"
           title="Delete" data-type="confirm"><i class="icon-trash"></i></a>
        @endcan
    </td>
</tr>


{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>Many desktop publishing packages and web</h6>--}}
        {{--<small>Created 18 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>18 Aug, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar" role="progressbar" aria-valuenow="78" aria-valuemin="0"--}}
                 {{--aria-valuemax="100" style="width: 78%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 78%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar10.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-success">Active</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>iNext - One Page Responsive Template</h6>--}}
        {{--<small>Created 14 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>22 Aug, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar l-slategray" role="progressbar" aria-valuenow="29"--}}
                 {{--aria-valuemin="0" aria-valuemax="100" style="width: 29%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 29%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-default">InActive</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>Massive Event - Conference and Event</h6>--}}
        {{--<small>Created 18 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>25 Sept, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100"--}}
                 {{--aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 100%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar10.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-danger">Closed</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>Oakleaf Admin - Bootstrap html5 with SASS</h6>--}}
        {{--<small>Created 18 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>29 Aug, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar" role="progressbar" aria-valuenow="13" aria-valuemin="0"--}}
                 {{--aria-valuemax="100" style="width: 13%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 13%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-success">Active</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>InfiniO 4.1</h6>--}}
        {{--<small>Created 14 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>05 Sept, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar" role="progressbar" aria-valuenow="48" aria-valuemin="0"--}}
                 {{--aria-valuemax="100" style="width: 48%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 48%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-success">Active</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>Many desktop publishing packages and web</h6>--}}
        {{--<small>Created 18 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>15 Sept, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar" role="progressbar" aria-valuenow="78" aria-valuemin="0"--}}
                 {{--aria-valuemax="100" style="width: 78%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 78%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar10.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar7.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-success">Active</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>InfiniO 4.2</h6>--}}
        {{--<small>Created 14 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>25 Sept, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="48"--}}
                 {{--aria-valuemin="0" aria-valuemax="100" style="width: 48%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 48%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-warning">Pending</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
    {{--<td class="project-title">--}}
        {{--<h6>Startup - OnePage Business Corporate Template</h6>--}}
        {{--<small>Created 14 July, 2018</small>--}}
    {{--</td>--}}
    {{--<td>26 Sept, 2018</td>--}}
    {{--<td>--}}
        {{--<div class="progress progress-xs">--}}
            {{--<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="56"--}}
                 {{--aria-valuemin="0" aria-valuemax="100" style="width: 56%;"></div>--}}
        {{--</div>--}}
        {{--<small>Completion with: 56%</small>--}}
    {{--</td>--}}
    {{--<td><img src="../assets/images/xs/avatar8.jpg" data-toggle="tooltip" data-placement="top"--}}
             {{--title="Team Lead" alt="Avatar" class="width35 rounded"></td>--}}
    {{--<td>--}}
        {{--<ul class="list-unstyled team-info">--}}
            {{--<li><img src="../assets/images/xs/avatar3.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
            {{--<li><img src="../assets/images/xs/avatar9.jpg" data-toggle="tooltip"--}}
                     {{--data-placement="top" title="Avatar"></li>--}}
        {{--</ul>--}}
    {{--</td>--}}
    {{--<td><span class="badge badge-warning">Pending</span></td>--}}
    {{--<td class="project-actions">--}}
        {{--<a href="project-detail.html" class="btn btn-sm btn-outline-primary"><i--}}
                {{--class="icon-eye"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-success"><i--}}
                {{--class="icon-pencil"></i></a>--}}
        {{--<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger js-sweetalert"--}}
           {{--title="Delete" data-type="confirm"><i class="icon-trash"></i></a>--}}
    {{--</td>--}}
{{--</tr>--}}
