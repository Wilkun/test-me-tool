@extends('layouts.auth')

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ action('ProjectController@index') }}">{{ __('Projects') }}</a></li>
    <li class="breadcrumb-item active"><a href="{{ action('ProjectController@show', ['id' => $project->id]) }}"></a>{{ $pTitle }}</li>
@endsection

@section('content')
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="body">
                <h5>{{ $project->name }}</h5>
                <p>{{ $project->description }}</p>
                <div class="progress-container progress-info m-b-25">
                    <span class="progress-badge">{{__('Project Status')}}</span>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 78%;">
                            <span class="progress-value">78%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <ul class=" list-unstyled basic-list">
                    <li>Created:<span class="badge-purple badge">{{ $project->created_at->diffForHumans() }}</span></li>
                    <li>Deadline:<span class="badge-purple badge">{{$project->deadline->diffForHumans()}}</span></li>
                    <li>Priority:<span class="badge-danger badge">Highest priority</span></li>
                    <li>Status<span class="badge-{{$project->status->slug}} badge">{{$project->status->name}}</span></li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>Assigned Team</h2>
            </div>
            <div class="body">
                <div class="w_user">
                    <img class="rounded-circle" src="{{$project->owner->get_gravatar($project->owner->email, 90)}}" alt="">
                    <div class="wid-u-info">
                        <h5>{{ $project->owner->name }}</h5>
                        <span>{{ $project->owner->email }}</span>
                        <p class="text-muted m-b-0">{{ __('Project Owner') }}</p>
                    </div>
                    <hr>
                </div>
                @if (count($members)>0)

                <ul class="right_chat list-unstyled mb-0">
                    @each('projects.member',$members,'member')
                </ul>
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Project Activity</h2>
            </div>
            <div class="body">
                <div class="form-group">
                    <textarea rows="2" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                </div>
                <div class="post-toolbar-b">
                    <button class="btn btn-warning"><i class="icon-paper-clip text-light"></i></button>
                    <button class="btn btn-warning"><i class="icon-camera text-light"></i></button>
                    <button class="btn btn-primary">Add</button>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div class="timeline-item green">
                    <span class="date">Just now</span>
                    <h6>iNext - One Page Responsive Template</h6>
                    <span>Project Lead: <a href="javascript:void(0);" title="Fidel Tonn">Fidel Tonn</a></span>
                </div>
                <div class="timeline-item warning">
                    <span class="date">02 Jun 2018</span>
                    <h6>Add Team Members</h6>
                    <span>By: <a href="javascript:void(0);" title="Fidel Tonn">Fidel Tonn</a></span>
                    <div class="msg">
                        <p>web by far While that's mock-ups and this is politics, are they really so different? I think the only card she has is the Lorem card.</p>
                        <ul class="list-unstyled team-info">
                            <li><img src="../assets/images/xs/avatar4.jpg" data-toggle="tooltip" data-placement="top" title="Chris Fox" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" data-placement="top" title="Joge Lucky" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" data-placement="top" title="Folisise Chosielie" alt="Avatar"></li>
                            <li><img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" data-placement="top" title="Joge Lucky" alt="Avatar"></li>
                        </ul>
                        <div class="top_counter">
                            <div class="icon"><i class="fa fa-file-word-o"></i> </div>
                            <div class="content">
                                <p class="mb-1">iNext project documentation.doc</p>
                                <span>Size: 2.3Mb</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-item warning">
                    <span class="date">02 Jun 2018</span>
                    <h6>Task Assigned</h6>
                    <span>By: <a href="javascript:void(0);" title="Fidel Tonn">Fidel Tonn</a></span>
                    <div class="msg">
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</p>
                        <div class="media">
                            <img class="media-object rounded width40 mr-3" src="../assets/images/xs/avatar1.jpg" alt="" />
                            <div class="media-body">
                                <h6 class="mb-0">Folisise Chosielie</h6>
                                <p class="mb-0"><strong>Detail:</strong> Ipsum is simply dummy text of the printing and typesetting industry. </p>
                            </div>
                        </div>
                        <div class="media">
                            <img class="media-object rounded width40 mr-3" src="../assets/images/xs/avatar5.jpg" alt="" />
                            <div class="media-body">
                                <h6 class="mb-0">Joge Lucky</h6>
                                <p class="mb-0"><strong>Detail:</strong> Ipsum is simply dummy text of the printing and typesetting industry. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-item warning">
                    <span class="date">02 Jun 2018</span>
                    <h6>Add new code on GitHub</h6>
                    <span>By: <a href="javascript:void(0);" title="Fidel Tonn">Folisise Chosielie</a></span>
                    <div class="msg">
                        <div class="alert alert-success mb-3" role="alert">Code Update Successfully in GitHub</div>
                        <pre class="prettyprint prettyprinted"><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-default"</span><span class="tag">&gt;</span><span class="pln">Default</span><span class="tag">&lt;/span&gt;</span><span class="pln">
</span><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-primary"</span><span class="tag">&gt;</span><span class="pln">Primary</span><span class="tag">&lt;/span&gt;</span><span class="pln">
</span><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-success"</span><span class="tag">&gt;</span><span class="pln">Success</span><span class="tag">&lt;/span&gt;</span><span class="pln">
</span><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-info"</span><span class="tag">&gt;</span><span class="pln">Info</span><span class="tag">&lt;/span&gt;</span><span class="pln">
</span><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-warning"</span><span class="tag">&gt;</span><span class="pln">Warning</span><span class="tag">&lt;/span&gt;</span><span class="pln">
</span><span class="tag">&lt;span</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"badge badge-danger"</span><span class="tag">&gt;</span><span class="pln">Danger</span><span class="tag">&lt;/span&gt;</span></pre>
                    </div>
                </div>
                <div class="timeline-item danger">
                    <span class="date">04 Jun 2018</span>
                    <h6>Project Reports</h6>
                    <span>By: <a href="javascript:void(0);" title="Fidel Tonn">Fidel Tonn</a></span>
                    <div class="msg">
                        <ul class="list-unstyled">
                            <li class="mb-2">
                                <span>Design Bug</span>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100" style="width: 17%;"></div>
                                </div>
                            </li>
                            <li class="mb-2">
                                <span>UI UX Design Task</span>
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="83" aria-valuemin="0" aria-valuemax="100" style="width: 83%;"></div>
                                </div>
                            </li>
                            <li class="mb-2">
                                <span>Developer Task</span>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="49" aria-valuemin="0" aria-valuemax="100" style="width: 49%;"></div>
                                </div>
                            </li>
                            <li class="mb-2">
                                <span>QA (Quality Assurance)</span>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="timeline-item dark">
                    <span class="date">05 Jun 2018</span>
                    <h6>Project on Goinng</h6>
                </div>
            </div>
        </div>
    </div>
@endsection
