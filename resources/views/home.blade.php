@extends('layouts.auth')

@section('breadcrumbs')
    <li class="breadcrumb-item active"></li>
@endsection

@section('content')
    <div class="card planned_task">
        <div class="header">
            <h2>Dashboard</h2>
            {{--<ul class="header-dropdown">--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>--}}
                    {{--<ul class="dropdown-menu dropdown-menu-right animated bounceIn">--}}
                        {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                        {{--<li><a href="javascript:void(0);">Another Action</a></li>--}}
                        {{--<li><a href="javascript:void(0);">Something else</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        </div>
        <div class="body">
            <h4>You are logged in!</h4>
        </div>
    </div>
@endsection
