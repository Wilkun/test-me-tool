<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .highlight {
            background-color: #f8f9fa;
            padding: 20px;
        }

        .highlight pre code {
            font-size: inherit;
            color: #212529;
        }

        .nt {
            color: #2f6f9f;
        }

        .na {
            color: #4f9fcf;
        }

        .s {
            color: #d44950;
        }

        pre.prettyprint {
            background-color: #eee;
            border: 0px;
            margin: 0;
            padding: 20px;
            text-align: left;
        }

        .atv,
        .str {
            color: #05AE0E;
        }

        .tag,
        .pln,
        .kwd {
            color: #3472F7;
        }

        .atn {
            color: #2C93FF;
        }

        .pln {
            color: #333;
        }

        .com {
            color: #999;
        }

    </style>
</head>
<body class="theme-orange">
<div id="wrapper">
    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
                <a href="{{ action('HomeController@index') }}"><img src="{{asset('images/logo.svg')}}"
                                                                    alt="{{ config('app.name', 'Laravel') }} Logo"
                                                                    class="img-responsive logo"></a>
            </div>

            <div class="navbar-right">
                <!--form id="navbar-search" class="navbar-form search-form">
					<input value="" class="form-control" placeholder="Search here..." type="text">
					<button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
				</form-->

                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <!--li><a href="app-events.html" class="icon-menu d-none d-sm-block d-md-none d-lg-block"><i class="icon-calendar"></i></a></li>
						<li><a href="app-chat.html" class="icon-menu d-none d-sm-block"><i class="icon-bubbles"></i></a></li>
						<li><a href="app-inbox.html" class="icon-menu d-none d-sm-block"><i class="icon-envelope"></i><span class="notification-dot"></span></a></li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="icon-bell"></i>
								<span class="notification-dot"></span>
							</a>
							<ul class="dropdown-menu notifications animated shake">
								<li class="header"><strong>You have 4 new Notifications</strong></li>
								<li>
									<a href="javascript:void(0);">
										<div class="media">
											<div class="media-left">
												<i class="icon-info text-warning"></i>
											</div>
											<div class="media-body">
												<p class="text">Campaign <strong>Holiday Sale</strong> is nearly reach budget limit.</p>
												<span class="timestamp">10:00 AM Today</span>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
										<div class="media">
											<div class="media-left">
												<i class="icon-like text-success"></i>
											</div>
											<div class="media-body">
												<p class="text">Your New Campaign <strong>Holiday Sale</strong> is approved.</p>
												<span class="timestamp">11:30 AM Today</span>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
										<div class="media">
											<div class="media-left">
												<i class="icon-pie-chart text-info"></i>
											</div>
											<div class="media-body">
												<p class="text">Website visits from Twitter is 27% higher than last week.</p>
												<span class="timestamp">04:00 PM Today</span>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
										<div class="media">
											<div class="media-left">
												<i class="icon-info text-danger"></i>
											</div>
											<div class="media-body">
												<p class="text">Error on website analytics configurations</p>
												<span class="timestamp">Yesterday</span>
											</div>
										</div>
									</a>
								</li>
								<li class="footer"><a href="javascript:void(0);" class="more">See all notifications</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-equalizer"></i></a>
							<ul class="dropdown-menu user-menu menu-icon animated bounceIn">
								<li class="menu-heading">ACCOUNT SETTINGS</li>
								<li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Basic</span></a></li>
								<li><a href="javascript:void(0);"><i class="icon-equalizer"></i> <span>Preferences</span></a></li>
								<li><a href="javascript:void(0);"><i class="icon-lock"></i> <span>Privacy</span></a></li>
								<li><a href="javascript:void(0);"><i class="icon-bell"></i> <span>Notifications</span></a></li>
								<li class="menu-heading">BILLING</li>
								<li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>Payments</span></a></li>
								<li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Invoices</span></a></li>
								<li><a href="javascript:void(0);"><i class="icon-refresh"></i> <span>Renewals</span></a></li>
							</ul>
						</li-->
                        <li><a class="icon-menu" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                    class="icon-login"></i></a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            @include('layouts.sidebar')
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                    class="fa fa-arrow-left"></i></a> {{ $pTitle }}</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ action('HomeController@index') }}"><i
                                        class="icon-home"></i></a></li>
                            @yield('breadcrumbs')
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                        @yield('sub-bar')
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                @if (session('status'))
                    <div class="card">
                        <div class="body">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
</html>
