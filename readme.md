# Test & Release Management Tool Logo

**TRMT** to projekt mający ułatwić pracę developerom i testerom.

## Ważne informacje
* gałąź `develop` **nie jest** uznawana za stabilną
* klonowanie tego  repozytorium  **nie jest** rekomendowane, chyba że chcesz pomóc w rozwoju

## Spis treści

[**TL;DR**](#markdown-header-tldr)

[**Instalacja i uruchomienie projektu**](#markdown-header-instalacja-projektu)

  * [**Konfiguracja**](#markdown-header-konfiguracja)
  * [**Uruchomienie**](#markdown-header-uruchomienie)

[**Cel projektu**](#markdown-header-cel-projektu)

**Dodatkowe Informacje**

  * [**Changelog**](#markdown-header-changelog)
  * [**Licencja**](#markdown-header-licencja)


## TL;DR
 TRMT bazuje na frameworku php [Laravel][laravel] [[dokumentacja][laravel-docs]], co znaczy że do działania wymaga wszystkich jego zależności.

## Instalacja projektu

Do działania projekt wymaga zainstalowanego środowiska PHP, nadzędzia Composer jak i połączenia z bazą danych. Wszelkie wskazówki znajdują się w dziale [Wiki: Instalacja Środowiska][wiki-env-installation].

W momencie gdy wszystkie te nadzędzia są dostępne, masz następujące opcje:

### `Opcja 1: Pobierz repozytorium`

> Nie masz zainstalowanego git-a, lecz chcesz przetestować

Pobierz plik `.zip` z tego [adresu][zip-download] i rozpakuj jego zawartość do folderu na dysku. Zapamiętaj ścieżkę do folderu.

### `Opcja 2: Sklonuj repozytorium`

> Masz zainstalowanego git-a, chcesz mieć dostęp do historii commitów

Uruchom terminal, następnie użyj komend:

```bash
#
cd SCIEŻKA-DO_FOLDERU-GDZIE-MA-BYĆ-PROJEKT
git clone https://bitbucket.org/Wilkun/test-me-tool.git
cd test-me-tool
git fetch && git checkout develop
```

### Konfiguracja

> Wymagana by projekt uruchomił się z odpowiednią bazą, schematem bazy, danymi

Zobacz [Wiki: Konfiguracja projektu][wiki-project-configuration]

### Uruchomienie

Projekt w celach developerskich i by zobaczyć go lokalnie uruchamiamy z poziomu terminala:


```sh
#
cd SCIEŻKA DO FOLDERU
php artisan serve
```

Po uruchomieniu projekt jest dostępny po adresem [`http://127.0.0.1:8000`](http://127.0.0.1:8000);

## Cel projektu

Zobacz [Wiki: Cel projektu][wiki-project-purpose]

## Changelog

Zobacz [changelog.md](changelog.md)

## Licencja

[MIT](LICENSE) © Irek Blicharski

<!-- linki -->
[laravel]:https://laravel.com/
[laravel-docs]:https://laravel.com/docs/5.7
[zip-download]:https://bitbucket.org/Wilkun/test-me-tool/get/develop.zip
[wiki-project-configuration]:https://bitbucket.org/Wilkun/test-me-tool/wiki/pages/configuration.md
[wiki-project-purpose]:https://bitbucket.org/Wilkun/test-me-tool/wiki/pages/purpose.md
[wiki-env-installation]:"https://bitbucket.org/Wilkun/test-me-tool/wiki/pages/env-installation.md"
