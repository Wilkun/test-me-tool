<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller {
    public function __constructor() {
        $this->middleware( 'auth' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = Auth::user();

        if($user->admin) {
            $projects = Project::where('deleted',0)->get();
        } else {
            $owned = $user->owned;
            $member = $user->projects;
            $projects = $owned->merge($member);
            $projects = $projects->all();
        }

        return view( 'projects.index' )->with( [
            'pTitle' => 'Projects List',
            'projects' => $projects
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', 'App\Models\Project');

        $project = new Project;

        $method = 'POST';

        $action = action('ProjectController@store');

        $pTitle = 'Create New Project';

        return view('projects.form')->with(compact('project', 'method', 'action', 'pTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {
        //
        $this->authorize('create', 'App\Models\Project');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Project $project ) {

        $this->authorize('view', $project);


        return view( 'projects.show' )->with( [
            'pTitle' => 'Project',
            'project' => $project,
            'members' => $project->members
        ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Project $project ) {
        $this->authorize('update', $project);

        $method = 'PUT';

        $action = action('ProjectController@update', ['id' => $project->id]);

        $pTitle = 'Edit this project';

        return view('projects.form')->with(compact('project', 'method', 'action', 'pTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, Project $project ) {
        $this->authorize('update', $project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( Project $project ) {
        $this->authorize('delete', $project);
        if($project->update(['deleted' => 1])) {
            return redirect('projects.index');
        }
    }
}
