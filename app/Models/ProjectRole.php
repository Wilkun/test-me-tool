<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectRole extends Model
{
    function users() {
        return $this->belongsToMany('App\Models\User')->using('App\Models\ProjectUser');
    }
}
