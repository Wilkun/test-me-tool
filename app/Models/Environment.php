<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Environment extends Model
{
    protected $fillable = [];

    protected  $hidden = [
        'created_at',
        'updated_at'
    ];

    public function projects(  ) {
        return $this->hasMany('App\Models\Project');
    }
}
