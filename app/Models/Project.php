<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Project extends Model
{
    protected $fillable = [
        'owner_id',
        'name',
        'status_id',
        'description'
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'deleted' => 'boolean',
        'created_at' => 'datetime:Y-m-d',
        'deadline' => 'datetime:Y-m-d',
    ];

    public function roles() {
        return $this->belongsToMany('App\Models\ProjectRole', 'project_user', 'project_id', 'role_id');
    }

    public function owner(){
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }

    public function environment(){
        return $this->belongsTo('App\Models\Environment');
    }

    public function status(  ) {
        return $this->hasOne('App\Models\Status', 'id', 'status_id');
    }

    public function members(){
        return $this->belongsToMany('App\Models\User', 'project_user', 'project_id', 'user_id')->withPivot('role_id');
    }

    public function isDeleted(  ) {
        return $this->deleted;
    }

    public function developers(){
        $role = ProjectRole::where('slug', 'dev')->first();
        return $this->belongsToMany('App\Models\User', 'project_user', 'project_id', 'user_id')->withPivot('role_id')->wherePivot('role_id', $role->id)->where('active',1);
    }

    public function testers(){
        $role = ProjectRole::where('slug', 'tester')->first();
        return $this->belongsToMany('App\Models\User', 'project_user', 'project_id', 'user_id')->withPivot('role_id')->wherePivot('role_id', $role->id)->where('active',1);
    }

    public function isOwner() {
        if($this->owner_id != Auth::id()){
            return false;
        }
        return true;
    }

    public function isMember(){
        $s = DB::table('project_user')->whereProjectId($this->id)->whereUserId(Auth::id())->get();
        if(count($s) == 0){
            return false;
        }
        return true;
    }
}
