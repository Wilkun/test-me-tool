<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
        'name', 'slug', 'icon'
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function projects(  ) {
        return $this->hasMany('App\Models\Project', 'status_id', 'id');
    }
}
